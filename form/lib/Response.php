<?php
namespace Marov;

use \PDO;

/**
 * Represents response to client
 */
class Response
{
    private $con;
    private $stmt;
    /**
     * Creates response
     * Throws PDOException
     */
    public function __construct()
    {
        $this->con = new PDO('mysql:host=sql111.hostfree.pw;dbname=epree_20373298_feedback', "epree_20373298", "p9(R;_kZ7");
        $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    
    /**
     * Executes transfering data to client
     * Throws PDOException
     */
    public function pullResponse()
    {
        $result = $this->con->query('SELECT * FROM feedbacks');
        $result->setFetchMode(PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            $row["message"] = htmlentities($row["message"]);
            
            print("<div class=\"message\"\n>");
            if($row["patronymic"]) {
               print("<div class=\"personalInfo\">" . $row["surname"] . " " . $row["name"] . " " . $row["patronymic"] . ", tel: " . $row["phone"] . "</div>\n");
            } else {
              print("<div class=\"personalInfo\">" . $row["surname"] . " " . $row["name"] . ", tel: " . $row["phone"] . "</div>\n");
            }
            print("<div class=\"messageText\">" . $row["message"] . "</div>\n");
            print("</div>");
        } 
    }
};
