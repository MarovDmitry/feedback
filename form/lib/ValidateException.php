<?php
/**
 * Contains custom exceptions
 */
namespace Marov;

use \Exception;

/**
 * Exception for incorrect format of input
 */
class ValidationException extends Exception
{
};
