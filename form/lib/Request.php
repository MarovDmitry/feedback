<?php
namespace Marov;

use \PDO;

/**
 * Represents request to server
 */
class Request
{
    private $name;
    private $surname;
    private $patronymic;
    private $phone;
    private $message;
    
    private $con;
    private $stmt;
    /**
    * Creates request.
    * Throws ValidationException, PDOException
    * @param string $name
    * @param string $surname
    * @param string $patronymic
    * @param string $phone
    * @param string $message
    */
    public function __construct($name, $surname, $patronymic, $phone, $message)
    {
        if (preg_match('/^[a-zA-Z]{1,15}$/', $name)) {
            $this->name = $name;
        } else {
            throw new ValidationException("name_error");
        }
        
        if (preg_match('/^[a-zA-Z]{1,15}$/', $surname)) {
            $this->surname = $surname;
        } else {
            throw new ValidationException("surname_error");
        }
        
        if (preg_match('/^[a-zA-Z]{0,15}$/', $patronymic)) {
          switch ($patronymic) {
            case '':
                $this->patronymic = null;
                break;  
            default:
                $this->patronymic = $patronymic;
                break;
          }
        } else {
            throw new ValidationException("patronymic_error");
        }
        
        if (preg_match('/^\d{10}$/', $phone)) {
            $this->phone = $phone;
        } else {
            throw new ValidationException("phone_error");
        }
        
        if (preg_match('/^.{0,1024}$/', $message))
        {
            $this->message = $message;
        } else {
            throw new ValidationException("message_error");
        }
        
        $this->con = new PDO('mysql:host=sql111.hostfree.pw;dbname=epree_20373298_feedback', "epree_20373298", "p9(R;_kZ7");
        $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->stmt=$this->con->prepare("INSERT INTO feedbacks(name, surname, patronymic, phone, message) VALUES (?,?,?,?,?);");
    }
    
    /**
     * Executes transaction with database
     */
    public function pushRequest()
    {
        $this->stmt->execute(array($this->name, $this->surname, $this->patronymic ,$this->phone, $this->message));
    }
};
