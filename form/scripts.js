/**
 * @fileoverview require jquery-3.2.1.js
 * @author d.marov94@gmail.com (Dmitry Marov)
 */

/**
 * Updates view from model
 */
function updateView() {
  $.post("form/response.php", {}, putData);
}

/**
 * Inserts data into view
 * @param {string} data
 * @param {string} textStatus
 */
function putData(data, textStatus) {
  $('#right').html(data);
}

/**
 * Inserts data into model
 */
function updateModel() {
  var name = $('#name').val();
  var surname = $('#surname').val();
  var patronymic = $('#patronymic').val();
  var phone = $('#phone').val();
  var message = $('#message').val();
  try {
    var r = new Request(name, surname, patronymic, phone, message);
    r.send();
  } catch(e) {
    var tip;
    switch(e.message) {
      case "name_error":
        tip = "Name is one word up to 15 symbols long (essential)";
        break;
      case "surname_error":
        tip = "Surname is one word up to 15 symbols long (essential)";
        break;
      case "patronymic_error":
        tip = "Patronymic is one word up to 15 symbols long (not essential)";
        break;
      case "phone_error":
        tip = "Telephone number is exactly 10 digits (essential)";
        break;
      case "message_error":
        tip = "Message should be up to 1024 symbols long";
        break;
      default:
        tip = "Unknown problem happened with your request";
        break;
    }
    alert(tip);
  }
}

/**
 * Creates valid request.
 * Throws {@code Error} if some parameters are not valid
 * @param {string} name
 * @param {string} surname
 * @param {string} patronymic
 * @param {string} phone
 * @param {string} message
 * @constructor
 */
function Request(name, surname, patronymic, phone, message)
{ 
  if ( /^[a-zA-Z]{1,15}$/.test(name) ) {
    /**
     * Name
     * @type {string}
     */
    this.name = name;
  } else {
    throw new Error("name_error");
  }
  
  if ( /^[a-zA-Z]{1,15}$/.test(surname) ) {
    /**
     * Surname
     * @type {string}
     */
    this.surname = surname;
  } else {
    throw new Error("surname_error");
  }
  
  if ( /^[a-zA-Z]{0,15}$/.test(patronymic) ) {
    /**
     * Patronymic
     * @type {string}
     */
    this.patronymic = patronymic;
  } else {
    throw new Error("patronymic_error");
  }
  
  if ( /^\d{10}$/.test(phone) ) {
    /**
     * Phone
     * @type {string}
     */
    this.phone = phone;
  } else {
    throw new Error("phone_error");
  }
    
  if ( /^.{0,1024}$/.test(message) ) {
    /**
     * Message
     * @type {string}
     */
    this.message = message;
  } else {
    throw new Error("message_error");
  }
/**
 * Sends request to server and updates view asynchroniously
 */
  this.send = function() {
    var array = {"name": this.name, "surname": this.surname, "patronymic": this.patronymic, "phone": this.phone, "message": this.message};
    $.post("form/request.php", array, updateView);
  };
}