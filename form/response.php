<?php
include_once('lib/Response.php');

try {
    $resp = new Marov\Response();
    $resp->pullResponse();
} catch(PDOException $e) {
    print('ERROR: ' . $e->getMessage());
} catch(Exception $e) {
    print('ERROR: ' . $e->getMessage());
}
