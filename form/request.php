<?php
include_once('lib/Request.php');

//all parameters
$name = filter_input(INPUT_POST, "name");
$surname = filter_input(INPUT_POST, "surname");
$patronymic = filter_input(INPUT_POST, "patronymic");
$phone = filter_input(INPUT_POST, "phone");
$message = filter_input(INPUT_POST, "message");
  
try {
    $req = new Marov\Request($name, $surname, $patronymic, $phone, $message);
    $req->pushRequest();
} catch (ValidationException $e) {
    print("ERROR: Server side validation failed<br>" . $e->getMessage());
} catch (PDOException $e) {
    print('ERROR: ' . $e->getMessage());
}
